Given (/^the following movies exist:$/) do |table|
  table.hashes.each do |movie|
	# each returned element will be a hash whose key is the table header.
	# we should arrange to add that movie to the database here.
    Movie.create!(movie)
  end
end

Then /(.*) seed movies should exist/ do | n_seeds |
  expect(Movie.count).to eq(n_seeds.to_i)
end

Then /I should see all the movies/ do
  rows = page.all('#movies tr').size - 1
  expect(rows).to eq(Movie.count())
end
